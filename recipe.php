<!DOCTYPE html>
<html>
<head>
	<title>Foodvourite | Your personal favorite dish tracker</title>
	<?php include('styles.php');?>
</head>
<body>
	<?php
	if(isset($_GET['recipe']) && !empty($_GET['recipe']))
	{
		$recipe = base64_decode($_GET['recipe']);
		foreach ($recipes as $key => $rec) {
			if($rec->name === $recipe){
				$recipeInfo = $rec;
				break;
			}
		}
	}
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12 col-md-12 col-12">
				<h1 class="title"><a href="javascript:;" onclick="goBack()" class="mdi mdi-arrow-left"></a> Best Food App</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-5 col-sm-12 col-12 my-3">
				<img src="<?php echo $recipeInfo->image; ?>" class="img-fluid" alt="">
			</div>
			<div class="col-xl-7 col-sm-12 col-12 my-3">
				<div class="row recipe-info">
					<div class="col-xl-9 col-sm-8 col-12">
						<h4><?php echo $recipeInfo->name; ?></h4>
						<h4><span class="mdi mdi-currency-inr"></span><?php echo $recipeInfo->price; ?></h4>
					</div>
					<div class="col-xl-3 col-sm-4 col-12 text-right">
						<div class="btn-group cart-btns">
							<button type="button" class="app-btn"><i class="mdi mdi-minus"></i></button>
							<span class="item-count">0</span>
							<button type="button" class="app-btn"><i class="mdi mdi-plus"></i></button>
						</div>
					</div>
				</div>
				<div class="row recipe-details my-3">
					<div class="col-xl-6 col-md-12 col-12">
						<p>Category : <?php echo $recipeInfo->category; ?></p>
					</div>
					<div class="col-xl-6 col-md-12 col-12">
						<p class="recipe-rating">
							<span class="mdi mdi-star"></span>
							<span class="rating"><?php echo $recipeInfo->rating; ?> Rating.</span>
							<span class="reviews"> (<?php echo $recipeInfo->reviews; ?> Reviews)</span>
						</p>
					</div>
				</div>
				<div class="row recipe-content my-3">
					<div class="col-xl-12 col-md-12 col-12">
						<h5>DETAILS</h5>
						<p><?php echo $recipeInfo->details; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
	<script>
	function goBack() {
	  window.history.back();
	}
	</script>
</body>
</html>