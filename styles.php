<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
	<meta name="description" content="">
	<meta name="keyword" content="">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/materialdesignicons.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap"/>
	<link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/responsive.css"/>
	<?php
		$food = json_decode(file_get_contents('http://temp.dash.zeta.in/food.php'));
		$categories = $food->categories;
		$recipes = $food->recipes;
		foreach ($recipes as $recipe) {
			if($recipe->isFavourite == 1)
				$favourites[] = $recipe;
		}
	?>