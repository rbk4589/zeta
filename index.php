<!DOCTYPE html>
<html>
<head>
	<title>Foodvourite | Your personal favorite dish tracker</title>
	<?php include('styles.php')?>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12 col-md-12 col-12">
				<h1 class="title">Best Food App</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row favourites">
			<div class="col-xl-10 col-md-10 col-10">
				<h3 class="sub-title">FAVOURITES</h3>
				<p>Enjoy what you have been ordering</p>
			</div>
			<div class="col-xl-2 col-md-2 col-2 text-right">
				<div class="cart d-flex justify-content-end align-items-end">
					<i class="mdi mdi-shopping" aria-hidden="true"></i>
					<label id="cart-count"></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="owl-carousel" id="favourites">
				<?php
				if(!empty($favourites)){
				foreach ($favourites as $key => $fav) {?>
				<div class="fav-dishes d-flex flex-column">
					<div class="fav-img d-flex">
						<img src="<?php echo $fav->image;?>" class="img-fluid rounded" alt="">
					</div>
					<div class="fav-details d-flex align-items-start justify-content-between">
						<div class="fav-price-name d-flex flex-column">
							<p><?php echo $fav->name ?></p>
							<p><span class="mdi mdi-currency-inr"></span><?php echo $fav->price ?></p>
						</div>
						<button type="button" class="btn app-btn">Reorder</button>
					</div>
				</div>
				<?php
				}
				}
				?>
			</div>
		</div>
		<div class="row my-4 search">
			<div class="col-md-12 col-12 d-flex">
				<span class="mdi mdi-magnify"></span>
				<input type="text" name="search" placeholder="Search for a particular dish or ingredient">
			</div>
		</div>
		<div class="row categories my-3">
			<div class="col-xl-10 col-md-9 col-9 d-flex justify-content-between align-items-center">
				<h3 class="sub-title">SELECT CATEGORIES</h3>
			</div>
			<div class="col-xl-2 col-md-3 col-3 d-flex justify-content-between align-items-center">
				<span>Filter</span>
				<span class="filter"><i class="mdi mdi-format-list-text" aria-hidden="true"></i></span>
			</div>
		</div>
		<div class="row cat-filter">
			<div class="owl-carousel" id="categories">
				<?php
				if(!empty($categories))
				{
				$i = 0;
				foreach ($categories as $key => $value) {?>
				<a href="javascript:;" onclick="filterCategory('<?php echo strtolower($value->name);?>')">
					<div class="category d-flex <?php echo $i == 0 ? 'active' : ''?>" id="<?php echo strtolower($value->name);?>">
						<div class="cat-items align-items-center d-flex">
							<img src="<?php echo $value->image;?>" class="img-fluid" alt="">
							<p><?php echo $value->name;?></p>
						</div>
					</div>
				</a>
				<?php
				$i++;
				}
				}
				?>
			</div>
		</div>
		<div class="row">
			<?php
			if(!empty($recipes)){
			foreach ($recipes as $key => $recipe) {?>
			<div class="col-xl-4 col-md-6 col-12 recipes d-none my-3 flex-column <?php echo strtolower($recipe->category);?>">
				<div class="fav-img d-flex">
					<a href="recipe.php?recipe=<?php echo base64_encode($recipe->name)?>" >
						<img src="<?php echo $recipe->image;?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="fav-details d-flex align-items-start justify-content-between">
					<div class="fav-price-name d-flex flex-column">
						<p><?php echo $recipe->name ?></p>
						<p><span class="mdi mdi-currency-inr"></span><?php echo $recipe->price ?></p>
					</div>
					<button type="button" class="btn app-btn add-to-cart">Add To Bag</button>
				</div>
			</div>
			<?php
			}
			}
			?>
		</div>
	</div>
	<script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		$(function() {
		  $('#favourites').owlCarousel({
		    margin: 0,
		    loop: false,
		    nav:false,
		    items:3,
		    stagePadding: 90,
		    responsive:{
		        0:{
		            items:1,
		        },
		        600:{
		            items:1,
		        },
		        768:{
		            items:1,
		        },
		        1000:{
		            items:3,
		        }
		    }
		  });
		  $('#categories').owlCarousel({
		    margin: 10,
		    loop: false,
		    nav:false,
		    items:2,
		    stagePadding: 90,
		    responsive:{
		        0:{
		            items:1,
		            nav:false,
		        },
		        400:{
		            items:1,
		            nav:false,
		        },
		        600:{
		            items:2,
		            nav:false,
		        },
		        800:{
		            items:2,
		            nav:false,
		        },
		        1000:{
		            items:4,
		            nav:false,
		        }
		    }
		  });
		});
	</script>
	<script type="text/javascript">
		const firstCat = document.getElementsByClassName("category")[0].id;
		const firstRecipe = document.getElementsByClassName(firstCat);
		for(let i=0;i<firstRecipe.length;i++)
		{
			firstRecipe[i].classList.add("d-flex")
			firstRecipe[i].classList.remove("d-none")
		}
		function filterCategory(cat)
		{
			const category = document.getElementsByClassName("category");
			for(let i=0;i<category.length;i++)
			{
				category[i].classList.remove("active")
			}
			document.getElementById(cat).classList.add("active");
			const recipes = document.getElementsByClassName("recipes");
			for(let i=0;i<recipes.length;i++)
			{
				recipes[i].classList.add("d-none")
				recipes[i].classList.remove("d-flex")
			}
			const activeRecipe = document.getElementsByClassName(cat);
			for(let i=0;i<activeRecipe.length;i++)
			{
				activeRecipe[i].classList.add("d-flex")
				activeRecipe[i].classList.remove("d-none")
			}
		}
	</script>
</body>
</html>